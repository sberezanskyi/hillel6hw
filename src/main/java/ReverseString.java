public class ReverseString {


    public static void main(String[] args) {
        System.out.println(reverseString(" GNUSMAS ,IMOAIX ,YNOS "));
    }


    public static String reverseString(String stroka) {
        char[] array = stroka.toCharArray();
        String result = "";
        for (int i = array.length - 1; i >= 0; i--) {
            result = result + array[i];
        }
        return result;
    }

}
